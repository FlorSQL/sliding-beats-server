class SongSerializer < ActiveModel::Serializer
  attributes :id, :title, :duration, :bpm, :album_id, :sync_moment, :artists

  def artists
    return SongsUser.where(song_id: id).order(:order).map(&:user)
  end
end
