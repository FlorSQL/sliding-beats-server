class Api::V1::ArtistsController < ApplicationController
  include ActionController::Serialization

  def index
    artists = User.where(is_artist: true)

    return render json: artists if params[:q].blank?

    return render json: artists.where('LOWER(stage_name) LIKE ?', "%#{params[:q].downcase}%")
  end
end