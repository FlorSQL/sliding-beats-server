class MyFailureApp < Devise::FailureApp
  def respond
    # send json errors everywhere
    if request.format != :json
      json_auth_failure
    else
      super
    end
  end

  def json_auth_failure
    self.status = 401
    self.content_type = 'application/json'
    self.response_body = { error: 'authentication error' }.to_json
  end
end