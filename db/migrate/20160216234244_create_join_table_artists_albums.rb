class CreateJoinTableArtistsAlbums < ActiveRecord::Migration
  def change
    create_join_table :artists, :albums do |t|
      t.index :artist_id
      t.index :album_id
    end
  end
end
