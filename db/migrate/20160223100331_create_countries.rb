class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.string :name
      t.string :name_in_local_language

      t.timestamps null: false
    end
  end
end
