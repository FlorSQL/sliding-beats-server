class Genre < ActiveRecord::Base
  validates :name, uniqueness: true
  has_and_belongs_to_many :albums
  has_and_belongs_to_many :songs
end
