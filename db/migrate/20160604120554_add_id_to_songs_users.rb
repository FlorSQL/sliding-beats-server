class AddIdToSongsUsers < ActiveRecord::Migration
  def change
    add_column :songs_users, :id, :primary_key
  end
end
