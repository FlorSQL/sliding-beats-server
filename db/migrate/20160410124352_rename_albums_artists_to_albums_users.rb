class RenameAlbumsArtistsToAlbumsUsers < ActiveRecord::Migration
  def change
    rename_table :albums_artists, :albums_users
  end
end
