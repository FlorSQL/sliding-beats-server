class AddOrderToPlaylistsSongs < ActiveRecord::Migration
  def change
    add_column :playlists_songs, :order, :integer
  end
end
