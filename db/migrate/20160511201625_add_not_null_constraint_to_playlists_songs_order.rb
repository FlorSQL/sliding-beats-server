class AddNotNullConstraintToPlaylistsSongsOrder < ActiveRecord::Migration
  def change
    change_column_null :playlists_songs, :order, true
  end
end
