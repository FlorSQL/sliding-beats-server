class Playlist < ActiveRecord::Base
  validates_presence_of :title

  belongs_to :user

  has_many :playlist_songs, dependent: :destroy
  has_many :songs, through: :playlist_songs
end
