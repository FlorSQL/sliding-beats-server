class Users::RegistrationsController < ApplicationController
  respond_to :json

  before_action :authenticate_user!, only: [:update]

  def create
    puts params
    params = user_params
    user = User.new(params)
    if user.save
      render :json=> user.as_json(:auth_token=>user.authentication_token, :email=>user.email), :status=>201
      return
    else
      warden.custom_failure!
      render :json=> user.errors, :status=>422
    end
  end

  def update
    san_params = user_params
    san_params[:stage_name] = nil if san_params[:stage_name].blank?

    # valid_password? checks if password given is current password
    unless current_user.valid_password?(san_params[:password])
      return render json: { errors: "Password entered is wrong" }, status: 403
    end

    san_params.except(:password).keys.each do |attr|
      current_user[attr] = san_params[attr]
    end

    new_password = params[:user][:new_password]
    current_user.password = new_password unless new_password.blank?

    if current_user.save
      return head 200
    else
      return render json: { errors: current_user.errors.full_messages }, status: 422
    end
  end

  private

  def user_params
    params.require(:user).permit(:first_name, :last_name, :is_artist, :stage_name, :email, :password)
  end
end