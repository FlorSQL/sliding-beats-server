class CreateJoinTableSongsUsers < ActiveRecord::Migration
  def change
    create_join_table :songs, :users do |t|
      # t.index [:song_id, :user_id]
      # t.index [:user_id, :song_id]
      t.integer :order
    end
  end
end
