class RenameSyncMomentToSyncMoment < ActiveRecord::Migration
  def change
    rename_column :songs, :syncMoment, :sync_moment
  end
end
