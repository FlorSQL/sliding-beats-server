# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160604120554) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "albums", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "albums_genres", id: false, force: :cascade do |t|
    t.integer "genre_id", null: false
    t.integer "album_id", null: false
  end

  add_index "albums_genres", ["album_id"], name: "index_albums_genres_on_album_id", using: :btree
  add_index "albums_genres", ["genre_id"], name: "index_albums_genres_on_genre_id", using: :btree

  create_table "albums_users", id: false, force: :cascade do |t|
    t.integer "user_id",  null: false
    t.integer "album_id", null: false
  end

  add_index "albums_users", ["album_id"], name: "index_albums_users_on_album_id", using: :btree
  add_index "albums_users", ["user_id"], name: "index_albums_users_on_user_id", using: :btree

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "genres", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "genres_songs", id: false, force: :cascade do |t|
    t.integer "genre_id", null: false
    t.integer "song_id",  null: false
  end

  add_index "genres_songs", ["genre_id"], name: "index_genres_songs_on_genre_id", using: :btree
  add_index "genres_songs", ["song_id"], name: "index_genres_songs_on_song_id", using: :btree

  create_table "playlist_songs", force: :cascade do |t|
    t.integer "song_id",     null: false
    t.integer "playlist_id", null: false
    t.integer "song_order"
  end

  add_index "playlist_songs", ["playlist_id"], name: "index_playlist_songs_on_playlist_id", using: :btree
  add_index "playlist_songs", ["song_id"], name: "index_playlist_songs_on_song_id", using: :btree

  create_table "playlists", force: :cascade do |t|
    t.string  "title"
    t.integer "user_id"
  end

  add_index "playlists", ["user_id"], name: "index_playlists_on_user_id", using: :btree

  create_table "songs", force: :cascade do |t|
    t.string   "title"
    t.integer  "duration"
    t.float    "bpm"
    t.integer  "album_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.float    "sync_moment"
  end

  add_index "songs", ["album_id"], name: "index_songs_on_album_id", using: :btree

  create_table "songs_after_songs", force: :cascade do |t|
    t.integer "initial_song_id"
    t.integer "next_song_id"
    t.integer "votes"
  end

  add_index "songs_after_songs", ["initial_song_id"], name: "index_songs_after_songs_on_initial_song_id", using: :btree
  add_index "songs_after_songs", ["next_song_id"], name: "index_songs_after_songs_on_next_song_id", using: :btree

  create_table "songs_users", force: :cascade do |t|
    t.integer "song_id", null: false
    t.integer "user_id", null: false
    t.integer "order"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "authentication_token"
    t.string   "unconfirmed_email"
    t.string   "first_name",                             null: false
    t.string   "last_name",                              null: false
    t.boolean  "is_artist",              default: false, null: false
    t.string   "stage_name"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "playlists", "users"
  add_foreign_key "songs", "albums"
  add_foreign_key "songs_after_songs", "songs", column: "initial_song_id"
  add_foreign_key "songs_after_songs", "songs", column: "next_song_id"
end
