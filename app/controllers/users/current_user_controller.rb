class Users::CurrentUserController < ApplicationController
  def show
    attributes = [:id, :first_name, :last_name, :is_artist, :stage_name, :email]
    unless current_user.blank?
      render json: current_user.as_json(only: attributes, root: :user)
    else
      render json: { user: nil }
    end
  end
end
