class AddIsArtistStageNameToUsers < ActiveRecord::Migration
  def change
    add_column :users, :is_artist, :boolean, default: false
    add_column :users, :stage_name, :string
  end
end
