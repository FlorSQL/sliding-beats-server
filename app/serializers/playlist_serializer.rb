class PlaylistSerializer < ActiveModel::Serializer
  attributes :id, :title, :song_ids

  def song_ids
    PlaylistSong.where(playlist_id: id).order(:song_order).map { |ps| ps.song.id }
  end
end
