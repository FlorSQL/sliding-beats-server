class AddIndexesToArtistsSongs < ActiveRecord::Migration
  def change
    add_index :artists_songs, :artist_id
    add_index :artists_songs, :song_id
  end
end
