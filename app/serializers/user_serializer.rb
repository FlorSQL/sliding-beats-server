class UserSerializer < ActiveModel::Serializer
  attributes :id, :stage_name, :first_name, :last_name
end
