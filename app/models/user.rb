  class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable

  before_save :ensure_authentication_token
  after_create :create_liked_playlist

  belongs_to :country
  has_many :playlists

  has_many :song_users, dependent: :destroy
  has_many :songs, through: :song_users

  has_and_belongs_to_many :albums

  validates_uniqueness_of :stage_name

  # because serializers aren't working with Devise...
  def as_json(options = {})
    super(only: [:id, :stage_name, :first_name, :last_name], root: :artist) if options.blank?
    super(options)
  end

  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
  end

  def send_devise_notification(notification, *args)
    devise_mailer.send(notification, self, *args).deliver_later
  end

  def liked_playlist
    return self.playlists.where(title: 'Liked')
  end

  private

  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).first
    end
  end

  def create_liked_playlist
    Playlist.create(user: self, title: 'Liked')
  end
end
