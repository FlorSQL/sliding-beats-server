class AddOrderToSongsUsers < ActiveRecord::Migration
  def change
    add_column :songs_users, :order, :integer
  end
end
