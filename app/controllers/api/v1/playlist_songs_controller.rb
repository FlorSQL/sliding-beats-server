class Api::V1::PlaylistSongsController < ApplicationController
  include ActionController::Serialization
  before_action :authenticate_user!

  def create
    playlist_song = PlaylistSong.new get_sanitized_params
    playlist_song.song_order = playlist_song.playlist.songs.length
    unless playlist_song.save
      return render json: { errors: playlist_song.errors.full_messages }
    end

    return head 204
  end

  def destroy_and_update_orders
    playlist_song = PlaylistSong.find_by({
      playlist_id: params[:playlist_id],
      song_id: params[:song_id]
    })

    order = playlist_song.song_order

    playlist_song.destroy
    if playlist_song.destroyed?
      PlaylistSong.where("playlist_id = #{params[:playlist_id]} AND song_order > #{order}").each do |ps|
        ps.song_order -= 1
        ps.save
      end
      return head 204
    else
      return render json: { errors: playlist_song.errors.full_messages }
    end

  end

  private

  def get_sanitized_params
    params.require(:playlist_song).permit(:playlist_id, :song_id)
  end
end
