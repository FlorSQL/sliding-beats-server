class AddForeignKeysToSongAfterSong < ActiveRecord::Migration
  def change
    add_foreign_key :table_song_after_songs, :songs, column: :initial_song_id
    add_foreign_key :table_song_after_songs, :songs, column: :next_song_id
  end
end
