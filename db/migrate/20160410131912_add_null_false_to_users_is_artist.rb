class AddNullFalseToUsersIsArtist < ActiveRecord::Migration
  def change
    change_column_null :users, :is_artist, false
  end
end
