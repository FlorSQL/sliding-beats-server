class RenameArtistsSongsToSongsUsers < ActiveRecord::Migration
  def change
    rename_table :artists_songs, :songs_users
  end
end
