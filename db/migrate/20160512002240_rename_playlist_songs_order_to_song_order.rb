class RenamePlaylistSongsOrderToSongOrder < ActiveRecord::Migration
  def change
    rename_column :playlist_songs, :order, :song_order
  end
end
