class RemoveScaleFromSongs < ActiveRecord::Migration
  def change
    remove_column :songs, :scale, :string
  end
end
