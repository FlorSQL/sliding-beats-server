class ChangeBpmToFloat < ActiveRecord::Migration
  def change
    change_column :songs, :bpm, :float
  end
end
