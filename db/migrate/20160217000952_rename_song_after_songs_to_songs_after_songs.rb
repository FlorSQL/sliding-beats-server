class RenameSongAfterSongsToSongsAfterSongs < ActiveRecord::Migration
  def change
    rename_table :table_song_after_songs, :songs_after_songs
  end
end
