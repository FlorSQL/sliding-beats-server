class RemoveNameInLocalLanguageFromCountries < ActiveRecord::Migration
  def change
    remove_column :countries, :name_in_local_language, :string
  end
end
