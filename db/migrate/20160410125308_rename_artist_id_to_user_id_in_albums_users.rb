class RenameArtistIdToUserIdInAlbumsUsers < ActiveRecord::Migration
  def change
    rename_column :albums_users, :artist_id, :user_id
  end
end
