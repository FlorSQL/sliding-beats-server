class Api::V1::SongsController < ApplicationController
  include ActionController::Serialization
  before_action :authenticate_user!, :check_if_owner_artist, only: [:upload]

  def index
    country = GeoIP2Compat.new("#{Rails.root}/db/GeoLite2-Country.mmdb")
                .lookup(request.remote_ip)[:country_name] rescue 'Romania'

    if params[:playlist_id].blank?
      songs = Song.all
    else
      songs = Playlist.find(params[:playlist_id])
                      .playlist_songs
                      .order(:song_order)
                      .map(&:song)
    end
    render json: songs

    # Song.all.joins(:playlist_songs).joins('LEFT OUTER JOIN playlists ON playlists.id = playlist_songs.song_id').where(playlists: {title: 'Liked'}).select('songs.*, count(playlists.id) as playlist_count').group('songs.id').order('playlist_count desc')

    # SELECT songs.*, count(playlists.id) as playlist_count
    # FROM "songs"
    # INNER JOIN "playlist_songs"
    # ON "playlist_songs"."song_id" = "songs"."id"
    # LEFT OUTER JOIN playlists ON playlists.id = playlist_songs.song_id
    # WHERE "playlists"."title" = $1
    # GROUP BY songs.id
    # ORDER BY playlist_count desc  [["title", "Liked"]]
  end

  def download
    send_file(get_song_path(params[:id]))
  end

  def upload
    song_params = permitted_song_params
    song_upload_params = permitted_song_upload_params

    song = Song.new(song_params)
    if song.save
      uploaded_file = song_upload_params[:file]
      File.open(Rails.root.join('music', "#{song[:id]}.mp3"), 'wb') do |file_io|
        begin
          file_io.write(uploaded_file.read)
        rescue => error
          file_io.close
          if File.exists?(get_song_path(song[:id]))
            File.delete (get_song_path(song[:id]))
          end

          song.destroy
          return head 422
        end
      end

      # artist_names = params[:artists].split('&&').map(&:squish)
      # artist_names.each do |artist_name|
      #   artist = User.where("LOWER(stage_name) = ?", "#{artist_name.downcase}").first

      #   if artist.blank?
      #     artist = User.new(stage_name: artist_name)
      #     unless artist.save
      #       return render json: { errors: artist.errors.full_messages }
      #     end
      #   end

      #   song.artists << artist
      # end

      @user_ids.each_with_index do |user_id, index|
        SongsUser.create(song: song, user_id: user_id, order: index)
      end

      album_title = params[:album]
      unless album_title.blank?
        album_title = album_title.squish
        album = Album.where("LOWER(title) = ?", "#{album_title.downcase}").first

        if album.blank?
          album = Album.new(title: album_title)
          unless album.save
            return render json: { errors: album.errors.full_messages }
          end
        end

        song.album = album
        # song.artists.each do |artist|
        #   album.artists << artist
        # end
        song.users << current_user
      end

      return render json: song
    end
  end

  private

  def get_song_path(song_id)
    return "#{Rails.root}/music/#{song_id}.mp3"
  end

  def permitted_song_params
    params.permit(:id, :title, :album_id, :bpm, :duration, :sync_moment)
  end

  def permitted_song_upload_params
    params.permit(:file, 'Content-Type')
  end

  def check_if_artist
    unless current_user.is_artist
      return render json: { errors: 'You\'re not authorized to do this' }, status: 401
    end
  end

  def check_if_owner_artist
    @user_ids = params[:artist_ids].split(',').map(&:to_i)
    if !current_user.is_artist || @user_ids.blank? || current_user.id != @user_ids.first
      return render json: { errors: 'You have to be the owner of the song' }, status: 401
    end
  end
end
