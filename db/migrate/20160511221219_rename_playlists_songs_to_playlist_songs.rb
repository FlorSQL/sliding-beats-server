class RenamePlaylistsSongsToPlaylistSongs < ActiveRecord::Migration
  def change
    rename_table :playlists_songs, :playlist_songs
  end
end
