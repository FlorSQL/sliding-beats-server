class Song < ActiveRecord::Base
  belongs_to :album
  has_and_belongs_to_many :genres

  has_many :songs_users, dependent: :destroy
  has_many :users, through: :songs_users

  has_many :playlist_songs, dependent: :destroy
  has_many :playlists, through: :playlist_songs
end
