class Api::V1::PlaylistsController < ApplicationController
  include ActionController::Serialization
  before_action :authenticate_user!

  def index
    return render json: Playlist.where(user_id: current_user.id)
  end

  def show
    playlist = Playlist.find(params[:id])

    if playlist.present?
      return render json: playlist
    else
      return head 404
    end

  end

  def create
    playlist_params = get_sanitized_params
    playlist = Playlist.new(playlist_params.merge({user_id: current_user.id}))

    if playlist.save
      reorder_songs_in_playlist(playlist, playlist_params[:song_ids])
      return render json: playlist
    else
      return render json: { errors: playlist.errors.full_messages }
    end
  end

  def update
    playlist_params = get_sanitized_params

    playlist = Playlist.find params[:id]
    return head :forbidden if playlist.user.id != current_user.id

    if playlist.update(playlist_params)
      reorder_songs_in_playlist(playlist, playlist_params[:song_ids])
      return head 204
    else
      return render json: { errors: playlist.errors.full_messages }
    end
  end

  def destroy
    playlist = Playlist.find params[:id]
    return head :forbidden if playlist.user.id != current_user.id

    if playlist.destroy
      return head 204
    else
      return render json: { errors: playlist.errors.full_messages }
    end
  end

  private

  def reorder_songs_in_playlist(playlist, song_ids)
    song_ids.each_with_index do |song_id, index|
        playlist.playlist_songs.where(song_id: song_id).each do |ps|
          ps.song_order = index
          ps.save
        end
      end
  end

  def get_sanitized_params
    san_params = params.require(:playlist).permit(:title, song_ids: [])
    san_params[:song_ids] ||= []
    return san_params
  end
end
