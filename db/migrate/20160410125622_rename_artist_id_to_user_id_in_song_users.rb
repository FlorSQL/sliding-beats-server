class RenameArtistIdToUserIdInSongUsers < ActiveRecord::Migration
  def change
    rename_column :songs_users, :artist_id, :user_id
  end
end
