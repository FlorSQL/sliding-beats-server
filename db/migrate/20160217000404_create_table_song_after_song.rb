class CreateTableSongAfterSong < ActiveRecord::Migration
  def change
    create_table :table_song_after_songs do |t|
      t.integer :initial_song_id, index: true
      t.integer :next_song_id, index: true
      t.integer :votes
    end
  end
end
