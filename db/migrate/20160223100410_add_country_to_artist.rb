class AddCountryToArtist < ActiveRecord::Migration
  def change
    add_reference :artists, :country, index: true, foreign_key: true
  end
end
